# Harbor

Installed via the Tanzu App Catalog

```
helm install --namespace  harbor --values harbor-helm-values.yml harbor tac/harbor
```
