# Sonarqube

Uses the Oteemo Helm charts.
https://github.com/Oteemo/charts/tree/master/charts/sonarqube


```
helm install --namespace sonarqube --values sonarqube-helm-values.yml sonarqube oteemocharts/sonarqube
```
