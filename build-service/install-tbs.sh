#!/bin/bash
TBS_DIR='tbs-1.1.3'
REGISTRY_ENDPOINT="harbor.e2e.tanzu.tacticalprogramming.com/tanzu-build-service/images"

# Prompt for credentials to registry
echo -n "Input registry username: "
read -s REGISTRY_USERNAME
echo -n "Input registry password: "
read -s REGISTRY_PASSWORD


# Do the thing
# ref: https://docs.pivotal.io/build-service/1-0/installing.html#other-install
ytt -f ${TBS_DIR}/values.yaml \
    -f ${TBS_DIR}/manifests/ \
    -v docker_repository="${REGISTRY_ENDPOINT}" \
    -v docker_username="${REGISTRY_USERNAME}" \
    -v docker_password="${REGISTRY_PASSWORD}" \
    | kbld -f ${TBS_DIR}/images-relocated.lock -f- \
    | kapp deploy -a tanzu-build-service -f- -y
