# k8s platform baseline

All files are normal Kubernetes manifest files meant to be used with Tanzu Kubernetes Grid.

For example, creating a Kubernetes cluster would look like this:
```
kubectl apply -f a-team-k8s-cluster.yml
```


The only exception is `homelab-wcp.yml` and files with the word `namespace` in them (e.g. `a-team-namespace.yml`. These files are meant to be used in conjunction with [wcpctl](https://github.com/papivot/wcpctl) to enable Workload Management on a vSphere installation and to create Tanzu Namespaces, respectively. Usage looks like this:

```
wcpctl.py create homelab-wcp.yml
wcpctl.py create a-team-namespace.yml
```

This is a very light, high-level example. The examples above assume you have already authenticated to the appropriate systems (vSphere, Kubernetes, etc).



# A note on Pod Security Policies
Due to the Secure by Default configuration of Tanzu Kubernetes Clusters, you'll need to create a clusterRoleBinding in order to deploy complex workloads. The easiest way to accomplish this is to issue the below command on a freshly-built k8s cluster:

```
kubectl create clusterrolebinding psp:authenticated --clusterrole=psp:vmware-system-privileged --group=system:authenticated 
```
