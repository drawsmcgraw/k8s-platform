# Excalidraw

[Excalidraw](https://excalidraw.com/) is lightweight whiteboarding software that runs in the browser. Perfect for facilitating conversations.
